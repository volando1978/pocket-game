﻿using UnityEngine;
using System.Collections;

public class cScriptMenu : MonoBehaviour
{

	
		//texture
		public Sprite manoT;
		public Sprite gotaT;
		public Sprite cerraduraT;
		public Sprite ojoT;
		public Sprite relojT;
		public Sprite arbolT;
		public Sprite bombillaT;
		public Sprite fuegoT;
		public Sprite piedraT;
		public Sprite trianguloT;
		public Sprite bocaT;
		public Sprite fantasmaT;
		SpriteRenderer spr;
	
		//estados
		public bool active = true;
		public bool moving = true;
		public bool bump = true;
		public bool scaled = false;
	
		public string propiedad;
	
	
	
	
		void Awake ()
		{
				spr = GetComponent<SpriteRenderer> ();
		}

	
		public Sprite setSpriteString (string p)
		{
				propiedad = p;
				print ("pro;: " + propiedad);
		
				if (propiedad.Equals ("ojo")) {
						spr.sprite = ojoT;
				}
				if (propiedad == "cerradura") {
						spr.sprite = cerraduraT;
			
				}
				if (propiedad == "gota") {
						spr.sprite = gotaT;
			
				}
				if (propiedad.Equals ("mano")) {
						spr.sprite = manoT;
			
				}
				if (propiedad == "reloj") {
						spr.sprite = relojT;
			
				}
				if (propiedad == "arbol") {
						spr.sprite = arbolT;
			
				}
				if (propiedad == "bombilla") {
						spr.sprite = bombillaT;
			
				}
				if (propiedad == "fuego") {
						spr.sprite = fuegoT;
			
				}
				if (propiedad == "piedra") {
						spr.sprite = piedraT;
			
				}
				if (propiedad == "triangulo") {
						spr.sprite = trianguloT;
			
				}
				if (propiedad == "boca") {
						spr.sprite = bocaT;
			
				}
				if (propiedad == "fantasma") {
						spr.sprite = fantasmaT;
			
				}
		
				return spr.sprite;
		}

	
		public void setPropiedad (string propiedad)
		{
				this.propiedad = propiedad;
		}

}
