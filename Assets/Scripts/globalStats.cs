﻿using UnityEngine;
using System.Collections;

public class globalStats : MonoBehaviour
{
		//Public Variables


		//Fraction defined by user that will limit the touch area
		public static int frac;
	
		//Private Variables
		private static float fracScreenWidth;
		private static float widthMinusFrac;
		private static Vector2 touchCache;
//		private static Vector2 mouseCache;


		private static bool touched = false;
		private static int screenHeight;
		private static int screenWidth;


		public static ArrayList myCollection = new ArrayList (); //guarda la propiedad de cada item
		public static ArrayList myCollectionStock = new ArrayList (); //guarda la cantidad de cada item

		void Awake ()
		{
				DontDestroyOnLoad (transform.gameObject);

				screenHeight = Screen.height;
				screenWidth = Screen.width;
				fracScreenWidth = screenWidth / frac;
				widthMinusFrac = screenWidth - fracScreenWidth;
		
		}
}
