﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour
{

		Ray ray;
		RaycastHit hit;
	
		// Update is called once per frame

		void Start ()
		{

		}
		void Update ()
		{
		
				#if UNITY_EDITOR
				//If mouse button 0 is down
				if (Input.GetMouseButton (0)) {
			
						Vector2 p = (Input.mousePosition);
						ray = Camera.main.ScreenPointToRay (p);

			
						if (Physics.Raycast (ray, out hit) && hit.collider.tag == "button") {
								GameObject g = hit.collider.gameObject;
								g.GetComponentInChildren<Animation> ().Play ();

						}
				}

		
#endif

				#if UNITY_IPHONE
				if (Input.touchCount >= 1) {
						foreach (Touch touch in Input.touches) {
			
								Vector2 t = (touch.position);
								ray = Camera.main.ScreenPointToRay (t);

								if (Physics.Raycast (ray, out hit) && hit.collider.tag == "button") {
										GameObject g = hit.collider.gameObject;
										g.GetComponentInChildren<Animation> ().Play ();
				
								}

						}
				}
#endif
		}
}
