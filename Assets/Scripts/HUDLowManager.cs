using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class HUDLowManager : MonoBehaviour
{

		public Camera camera;
		private float _camX;
		private float _camY;
		private float _camZ;

		public float posYsquare ; //0.6
		public float posXsquareOffset;//0.28
		public float posXsquareBetween;//0.14
		public float square_depth;//-18.8

		public float selectedItemSizeScale;//0.06
		
		public ArrayList lowHUD = new ArrayList ();
		public ArrayList lowHUDCount = new ArrayList ();

		public GUIStyle st = new GUIStyle ();

		public GameObject hudSquare ;
		public int nHUDSquare;
		public List<GameObject> lowHudSquareArray = new List<GameObject> ();





		public void throwLowHUDSquare ()
		{
		
				for (int i=0; i< nHUDSquare; i++) {
			
						Vector3 HUDsquarePos = new Vector3 ((_camX - posXsquareOffset) + posXsquareBetween * i, _camY + posYsquare, square_depth);
						GameObject square = Instantiate (hudSquare, HUDsquarePos, Quaternion.identity) as GameObject;
						square.transform.parent = GameObject.Find ("HUDLow").transform;//tag mejor
						lowHudSquareArray.Add (square);

				}
		
		}
	
		// Update is called once per frame
		void Update ()
		{
				_camX = camera.transform.position.x;
				_camY = camera.transform.position.y;
				_camZ = camera.transform.position.z;
		}

		public bool checkRepeatedInLowHUD (GameObject g)
		{

				bool repeated = false;

				for (int i = 0; i< lowHUD.Count; i++) {
						GameObject g2 = (GameObject)lowHUD [i];

						//si es el mismo tipo de bola
						if (g.GetComponent<cScript> ().propiedad == g2.GetComponent<cScript> ().propiedad) {

								repeated = true;
				
						} 
			
				}
				return repeated;
		}

		public void incrementaStock (GameObject g)
		{

				for (int i = 0; i< lowHUD.Count; i++) {
						GameObject g2 = (GameObject)lowHUD [i];
			
						//si es el mismo tipo de bola
						if (g.GetComponent<cScript> ().propiedad == g2.GetComponent<cScript> ().propiedad) {
				
								int increment = (int)lowHUDCount [i];
								lowHUDCount [i] = increment + 1;
		
//								print ("incrementa stock: " + increment + " en " + lowHUD [i].ToString () + " | " + lowHUDCount [i].ToString ());
						}
				}
		}
	
	
		public void moveToLowHUD (GameObject g, itemManager itM)
		{

				itemManager _itemManager = itM.GetComponent<itemManager> ();
				lowHUD.Add (g);
				lowHUDCount.Add (1);

				globalStats.myCollection.Add (g.GetComponent<cScript> ().propiedad);
				globalStats.myCollectionStock.Add (1);


				for (int i = 0; i< lowHUD.Count; i++) {

						float xPos = lowHudSquareArray [i].transform.position.x;// + posXsquareBetween * i;
						float yPos = lowHudSquareArray [i].transform.position.y;
						float zPos = lowHudSquareArray [i].transform.position.z + 1f;

						Vector3 squarePos = new Vector3 (xPos, yPos, zPos);
						Vector3 scale = new Vector3 (selectedItemSizeScale, selectedItemSizeScale);
						g.transform.position = squarePos;
						g.transform.localScale = scale;
						g.GetComponent<SphereCollider> ().enabled = false;
						g.transform.parent = this.transform;
			
			
				}
		
		}
	

		public bool checkStock ()
		{
				bool isComplete = false;
//				print ("Cuentas: " + itemArrayLowHUD.Count + "      " + cScript.magias.Count);
				if (lowHUD.Count >= itemManager.currentMagias.Count) {
						isComplete = true;

				}
				return isComplete;

		}

		public bool checkSet (int nSet)
		{
				bool isSetCompleted = false;
				if (lowHUD.Count % nSet == 0) {
						isSetCompleted = true;
				}
				return isSetCompleted;

		}

		public void deleteLowHUD ()
		{

				lowHUD.Clear ();
				lowHUDCount.Clear ();
				print ("cleaned: " + lowHUD.Count + "  " + lowHUDCount);

		}

		public void cleanHUD ()
		{
				foreach (GameObject g in lowHUD) {
						for (int i = 0; i< lowHUD.Count; i++) {
								globalStats.myCollection.Add ((GameObject)lowHUD [i]);
								globalStats.myCollectionStock.Add ((int)lowHUDCount [i]);
//								g.renderer.enabled = false;
//								Destroy (g);
						}
				}
	
		}


}
