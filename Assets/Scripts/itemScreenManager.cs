﻿using UnityEngine;
using System.Collections.Generic;

public class itemScreenManager : MonoBehaviour
{

		public GameObject hud_squareObj;
		public GameObject itemObj;

		public int nX;
		public int nY;

		public float distanceBetween;
		List<GameObject> gridArray = new List<GameObject> ();

		public float speed;


		public Vector3 initPos;
		public bool initPosLocked;
		public Vector3 selectedItemPos;

		public GameObject selectedItem;

		void Start ()
		{
				initGrid ();
				fillGrid ();

		}

		public void getInitPos ()
		{
				if (!initPosLocked) {

						initPos = selectedItem.transform.position;
						initPosLocked = true;
				}

		}

		public void clearInitPos ()
		{
				initPosLocked = false;
		}
	
		// Update is called once per frame
		void Update ()
		{

				#if UNITY_EDITOR
				//If mouse button 0 is down
				if (Input.GetMouseButton (0)) {

						Vector2 p = (Input.mousePosition);
						Ray ray = Camera.main.ScreenPointToRay (p);
						RaycastHit hit;


						if (Physics.Raycast (ray, out hit) && hit.collider.tag == "item") {
								selectedItem = hit.collider.gameObject;
								cScript _selectedItem = selectedItem.GetComponent<cScript> ();
								selectedItem.GetComponent<SpriteRenderer> ().color = Color.blue;
								getInitPos ();

						} 

						Vector3 mouseCache = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 1);
						selectedItemPos = Camera.main.ScreenToWorldPoint (mouseCache);
						if (selectedItem) {
								selectedItem.transform.position = Vector3.Lerp (selectedItem.transform.position, selectedItemPos, Time.deltaTime * 100f);
						}	

				}
				if (selectedItem && !Input.GetMouseButton (0)) {

						selectedItem.transform.position = Vector3.Lerp (selectedItem.transform.position, initPos, Time.deltaTime * 100f);
						selectedItem.GetComponent<SpriteRenderer> ().color = Color.white;
						selectedItem = null;
						clearInitPos ();
				}



#endif
				#if UNITY_IPHONE

				if (Input.touchCount >= 1) {
						foreach (Touch touch in Input.touches) {

								Vector2 t = (touch.position);
								Ray ray = Camera.main.ScreenPointToRay (t);
								RaycastHit hit;
			
								if (Physics.Raycast (ray, out hit) && hit.collider.tag == "item") {
										selectedItem = hit.collider.gameObject;
										cScript _selectedItem = selectedItem.GetComponent<cScript> ();
										selectedItem.GetComponent<SpriteRenderer> ().color = Color.blue;
										getInitPos ();

								} 
			
								Vector3 touchCache = new Vector3 (touch.position.x, touch.position.y, 1);
								selectedItemPos = Camera.main.ScreenToWorldPoint (touchCache);
								if (selectedItem) {
										selectedItem.transform.position = Vector3.Lerp (selectedItem.transform.position, selectedItemPos, Time.deltaTime * 100f);
								}
						}
				}

				if (selectedItem && Input.touchCount < 1) {
						
						selectedItem.transform.position = Vector3.Lerp (selectedItem.transform.position, initPos, Time.deltaTime * 100f);
						selectedItem.GetComponent<SpriteRenderer> ().color = Color.white;
						selectedItem = null;
						clearInitPos ();

			
				}
				#endif


		}
		
		

		public void initGrid ()
		{

				for (int i =0; i<nX; i++) {
						for (int j=0; j<nY; j++) {
								Vector3 myPos = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
								Vector3 v = new Vector3 (myPos.x + distanceBetween * i, myPos.y - distanceBetween * j, Camera.main.transform.position.z + 1);
								GameObject gridSquare = (GameObject)Instantiate (hud_squareObj, v, Quaternion.identity);
								gridSquare.transform.parent = transform;
								gridArray.Add (gridSquare);
						}
				} 


		}

		public void fillGrid ()
		{

//				for (int i =0; i<globalStats.myCollection.Count; i++) {
//			
//						print ("he33 " + (GameObject)globalStats.myCollection [i] + "n: " + i);
//				}

				for (int i =0; i<globalStats.myCollection.Count; i++) {

						
						Vector3 itemPos = (Vector3)gridArray [i].transform.position;
						GameObject g = (GameObject)Instantiate (itemObj, itemPos, Quaternion.identity);//(GameObject)globalStats.myCollection [i];
						
						Vector3 scale = new Vector3 (0.04f, 0.04f, 0.04f);
						
						g.transform.localScale = scale;
						g.GetComponent<cScriptMenu> ().setPropiedad (globalStats.myCollection [i].ToString ());
						print ("h:  :  " + g.GetComponent<cScriptMenu> ().propiedad);

						Sprite sp = g.GetComponent<cScriptMenu> ().setSpriteString (g.GetComponent<cScriptMenu> ().propiedad.ToString ());
						g.GetComponent<cScriptMenu> ().moving = false;
						g.GetComponent<cScriptMenu> ().active = false;

						
				}




		}

		public void destroyGrid ()
		{

		}

}
