﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class stencilManager : MonoBehaviour
{
		public GameObject stencil;
		public int nStencils; // 10
		public List<GameObject> stencilArray = new List<GameObject> ();
		public float maxTilt;//360 rotacion panos
		public float minTilt;
		public float maxOffset;
		public float minOffset;
		public int depth; //20
		public float speed;//-0.2 velocidad panos
		public float offSetLerpCam;//3 velocidad de traslacion camara

		public Camera camera;
		private float _camX;
		private float _camY;
		private float _camZ;

		public float darkness = 0.9f;
		public static Color stencilColor;

		public enum state
		{
				f1,
				f2,
				f3,
				f4,
				f5,
				f6,
				f7,
				f8,
				f9,
				f0,
				feedback}
		;
	
		public static state currentState = state.feedback;
		public static state prevState = state.f1;
	
		// Use this for initialization
		void Start ()
		{
				_camZ = camera.transform.position.z;
				camera = Camera.main;
				assignSprite (stencil);
				throwStencils ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				_camX = camera.transform.position.x;
				_camY = camera.transform.position.y;
		
				//movimiento de los stencils en el background
				for (int i=0; i< stencilArray.Count; i++) {
			
						Vector3 currPos = new Vector3 (stencilArray [i].transform.position.x, stencilArray [i].transform.position.y, stencilArray [i].transform.position.z);
						Vector3 vSet = new Vector3 (_camX, _camY, stencilArray [i].transform.position.z);
			
						stencilArray [i].transform.position = Vector3.Lerp (currPos, vSet, Time.deltaTime * offSetLerpCam);
			
			
						Vector3 vMove = new Vector3 (0, 0, speed);
						stencilArray [i].transform.Translate (vMove);

						// si z < cam.z
						if (stencilArray [i].transform.position.z < _camZ) {

								assignSprite ((GameObject)stencilArray [i]);
								assignTras ((GameObject)stencilArray [i]);
								assingRot ((GameObject)stencilArray [i]);
								assignColor ((GameObject)stencilArray [i]);
						}
				}

				if (currentState == state.feedback) {

						StartCoroutine ("timing");

				}

		
		}
		public void assignSprite (GameObject g)
		{
				SpriteRenderer r = g.GetComponent<SpriteRenderer> ();

				if (currentState == state.f1) {
						r.sprite = g.GetComponent<stencil> ().spr0;
				}
				if (currentState == state.f2) {
						r.sprite = g.GetComponent<stencil> ().spr1;
				}

				if (currentState == state.feedback) {
		
						r.sprite = g.GetComponent<stencil> ().spr9;

				}
		
		}
		public void assignTras (GameObject g)
		{
				Vector3 vNew = new Vector3 (0, 0, _camZ + depth);
				g.transform.position = vNew;
		}

		public void assingRot (GameObject g)
		{
				Vector3 rotPos = new Vector3 (0, 0, Random.Range (maxTilt, minTilt));
				g.transform.Rotate (rotPos);
		}

		public void assignColor (GameObject g)
		{
				SpriteRenderer r = g.GetComponent<SpriteRenderer> ();


//				stencilColor = new Color (0f, 0f, 0f, 1);//mod to get 0.5 to 1
		
//				if (currentState == state.f1) {

//				float randomRangeColor = Random.Range (0f, 1f);
//				c = new Color (randomRangeColor, randomRangeColor, randomRangeColor, 1);//mod to get 0.5 to 1

//				}

//				if (currentState == state.feedback) {
				float randomRangeColorR = Random.Range (0f, darkness);
				float randomRangeColorG = Random.Range (0f, darkness);
				float randomRangeColorB = Random.Range (0f, darkness);
				stencilColor = new Color (randomRangeColorR, randomRangeColorG, randomRangeColorB, 1);//mod to get 0.5 to 1

			
//				}
				r.color = stencilColor;
			
		}
	
		public void throwStencils ()
		{
				for (int i=0; i< nStencils; i++) {
			
						Vector3 pos = new Vector3 (0, 0, i * 2);
						GameObject clone = Instantiate (stencil, pos, Quaternion.identity) as GameObject;
						stencilArray.Add (clone);
			
				}
		}

		public IEnumerator timing ()
		{
				yield return new WaitForSeconds (2f); // waits 3 seconds
				currentState = prevState;

		}
}
