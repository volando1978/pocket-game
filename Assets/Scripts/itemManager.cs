using UnityEngine;
using System.Collections;

public class itemManager : MonoBehaviour
{

		public Camera camera;
		private float _camX;
		private float _camY;
		private float _camZ;

		public GameObject item;
		public ArrayList hud = new ArrayList ();

		public static int nMagias = 0;
		public static string[]  magiasproxy = {
		"mano",
		"cerradura",
		"gota",
		"reloj",
		"ojo",
		"arbol",
		"boca",
		"bombilla",
		"fuego",
		"piedra",
		"triangulo",
		"fantasma"
	};
		public static ArrayList currentMagias = new ArrayList ();//{"mano", "cerradura", "gota","reloj", "ojo"} ;
	

		// Use this for initialization
		void Start ()
		{
				_camZ = camera.transform.position.z;
				camera = Camera.main;
				throwItem ();


		}
	


		public void throwItem ()
		{

				_camX = camera.transform.position.x;
				_camY = camera.transform.position.y - 1f;
				_camZ = camera.transform.position.z;

				Vector3 itemPos = new Vector3 (_camX, _camY, _camZ + 2);
				GameObject it = Instantiate (item, itemPos, Quaternion.identity) as GameObject;

				it.transform.parent = Camera.main.transform;//GameObject.Find ("itemManager").transform;
				updateMagias ();

		
		}


		public void destroyItemsInArray (ArrayList itArr)
		{
				for (int i = 0; i< hud.Count; i++) {
						Destroy ((Object)hud [i]);
				}
		}



		public static void increaseNMagias ()
		{
				if (nMagias < magiasproxy.Length)
						nMagias += 1;

//				print ("incrementa Magia: " + "n: " + nMagias);

				stencilManager.currentState = stencilManager.state.feedback;
		}
	
		public static void updateMagias ()
		{
				currentMagias.Clear ();

				for (int i = Manager.levelSet; i<= nMagias; i++) {
						currentMagias.Add (magiasproxy [i]);
						//Si nMagias es mayor que el set, cambia de fase AQUI
			
				}
		}


	

}
