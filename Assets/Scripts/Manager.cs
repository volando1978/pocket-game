﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Manager : MonoBehaviour
{


		public Camera camera;
		private float _camX;
		private float _camY;
		private float _camZ;

		//stencils
		public GameObject stencilManager;
		public stencilManager _stencilManager;

		//HUD
		public GameObject HUD;
		public HUDManager _HUDManager;

		//Items
		public GameObject itemManagerObj;
		public itemManager _itemManager;

		//LowHUD
		public GameObject HUDLow;
		public HUDLowManager _HUDLowManager ;

		//monedero
		public GameObject monedero;
		public monederoManager _monederoManager ;

		public GameObject itemTallyHUD;
		public cScript _itemTallyHUD;

		public GameObject splashObj;
		public ParticleSystem _splash;

		public GameObject sceneManagerObj;
		public SceneManager _sceneManager;

		public static int nSet = 3;
		public static int levelSet = 0;





	


		// Use this for initialization
		void Start ()
		{
				_camZ = camera.transform.position.z;
				camera = Camera.main;

				_stencilManager = stencilManager.GetComponent<stencilManager> ();
				_HUDManager = HUD.GetComponent<HUDManager> ();
				_HUDManager.throwHUDSquare ();

				_itemManager = itemManagerObj.GetComponent<itemManager> ();

				_HUDLowManager = HUDLow.GetComponent<HUDLowManager> ();
				_HUDLowManager.throwLowHUDSquare ();

				_monederoManager = monedero.GetComponent<monederoManager> ();
				_itemTallyHUD = itemTallyHUD.GetComponent<cScript> ();
//				_splash = splashObj.GetComponent<particleSystem> ();
				_sceneManager = sceneManagerObj.GetComponent<SceneManager> ();
		
		}
	
		// Update is called once per frame
		void Update ()
		{


				if (Input.GetButtonDown ("Fire2")) {

						_sceneManager.loadScene ("itemScreen");
				}
		
//				if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
//						Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);

				if (Input.GetButtonDown ("Fire1")) {

						Vector2 p = (Input.mousePosition);
						Ray ray = Camera.main.ScreenPointToRay (p);

						RaycastHit hit;
			
						print ("ray");

						//toca item
						if (Physics.Raycast (ray, out hit) && hit.collider.tag == "item") {
				
								GameObject selectedItem = hit.collider.gameObject;
								cScript _selectedItem = selectedItem.GetComponent<cScript> ();

								// si el HUD  esta lleno y el objeto esta activo
								if (isRackComplete ()) {

										// si la combinacion existe / devuelve un objeto
										if (CheckCombination (_itemManager.hud)) {
												GameObject go = (GameObject)Instantiate (getRepeatedObject (_itemManager.hud));										
												
												//si no esta repetido en la combinacion de abajo
												if (!_HUDLowManager.checkRepeatedInLowHUD (go)) {
														
														// lo mete en la coleccion, lo representa abajo
														_HUDLowManager.moveToLowHUD (go, _itemManager);
//														_HUDLowManager.checkStock ();

														if (_HUDLowManager.checkSet (nSet)) {
																_sceneManager.loadScene ("itemScreen");
														}

														itemManager.increaseNMagias ();
														_itemTallyHUD.setItemTally ();

														// si esta repetido en la combinacion
												} else {
														_HUDLowManager.incrementaStock (go);
												}
										}
										//ejecuta bono
										// resta credito
										_itemManager.destroyItemsInArray (_itemManager.hud);
					
										//resetea array de items
										_itemManager.hud.Clear ();
								

										//si HUD NO completo
								} else {

										_selectedItem.GetComponent<cScript> ().stopMoving ();
										_selectedItem.GetComponent<cScript> ().active = false;
										_selectedItem.GetComponent<cScript> ().moving = false;
//										_splash.particleSystem.enableEmission = true;

										_HUDManager.moveToHUD (selectedItem, HUD, itemManagerObj);
										_itemManager.throwItem ();
										_monederoManager.decreaseCreditos ();

								}

						}

				}
		}

		public bool isRackComplete ()
		{
				bool b = true;

				if (_itemManager.hud.Count < _HUDManager.nHUDSquare) {
						b = false;
				}
				return b;
		}

		//devuelve si/no esta repetido
		public bool CheckCombination (ArrayList itemArray)
		{

				GameObject aO = (GameObject)itemArray [0];
				GameObject bO = (GameObject)itemArray [1];
				GameObject cO = (GameObject)itemArray [2];
//				GameObject dO = (GameObject)itemArray [3];
//				GameObject eO = (GameObject)itemArray [4];
		
		
				string a = aO.GetComponent<cScript> ().propiedad;
				string b = bO.GetComponent<cScript> ().propiedad;
				string c = cO.GetComponent<cScript> ().propiedad;
//				string d = dO.GetComponent<cScript> ().propiedad;
//				string e = eO.GetComponent<cScript> ().propiedad;

				bool combinationFound = false;
//				GameObject gameObjectToReturn = null;
		
				if (!combinationFound && a.Equals (b) && a.Equals (c)) {
//						print (a + b + c);
						combinationFound = true;

//						gameObjectToReturn = aO;
				}

//				if (!combinationFound && b.Equals (c) && b.Equals (d)) {
//						print (a + b + c);
//						combinationFound = true;
////						gameObjectToReturn = bO;
//				}
//
//				if (!combinationFound && c.Equals (d) && c.Equals (e)) {
//						print (a + b + c);
//						combinationFound = true;
////						gameObjectToReturn = cO;
//				}

//				if (gameObjectToReturn != null) {
//						return gameObjectToReturn;
//				} else {
				return combinationFound;
//				}
		}

		//devuelve el objeto repetido
		public GameObject getRepeatedObject (ArrayList itemArray)
		{

				GameObject aO = (GameObject)itemArray [0];
				GameObject bO = (GameObject)itemArray [1];
				GameObject cO = (GameObject)itemArray [2];
//				GameObject dO = (GameObject)itemArray [3];
//				GameObject eO = (GameObject)itemArray [4];
		
		
				string a = aO.GetComponent<cScript> ().propiedad;
				string b = bO.GetComponent<cScript> ().propiedad;
				string c = cO.GetComponent<cScript> ().propiedad;
//				string d = dO.GetComponent<cScript> ().propiedad;
//				string e = eO.GetComponent<cScript> ().propiedad;
		
				bool combinationFound = false;
				GameObject gameObjectToReturn = null;
		
				if (!combinationFound && a.Equals (b) && a.Equals (c)) {
//						print (a + b + c);
						combinationFound = true;
			
						gameObjectToReturn = aO;
				}
		
//				if (!combinationFound && b.Equals (c) && b.Equals (d)) {
////						print (a + b + c);
//						combinationFound = true;
//						gameObjectToReturn = bO;
//				}
//		
//				if (!combinationFound && c.Equals (d) && c.Equals (e)) {
////						print (a + b + c);
//						combinationFound = true;
//						gameObjectToReturn = cO;
//				}
		
				//				if (gameObjectToReturn != null) {
				//						return gameObjectToReturn;
				//				} else {
				return gameObjectToReturn;
				//				}
		}


	
}
