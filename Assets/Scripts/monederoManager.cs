﻿using UnityEngine;
using System.Collections;

public class monederoManager : MonoBehaviour
{

		public static int creditos;
		public int initialCreditos;


		// Use this for initialization
		void Start ()
		{
				updateCreditos ();
		}

		public void updateCreditos ()
		{
				creditos = initialCreditos;
		}

		public void decreaseCreditos ()
		{
				if (creditos > 1) {

						creditos -= 1;
				}
		}

		public void increaseCreditos (int q)
		{
				creditos += q;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}


}
