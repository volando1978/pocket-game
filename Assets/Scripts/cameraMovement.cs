﻿using UnityEngine;
using System.Collections;

public class cameraMovement : MonoBehaviour
{
		public float speed = 0.000001f;
		public float offSetDelta;
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
				#if UNITY_EDITOR

//				Vector3 vNew = new Vector3 (0, 0, speed);
//				transform.Translate (vNew);

//				float vertical = Input.GetAxis ("Mouse Y");
//				float horizontal = Input.GetAxis ("Mouse X");
//
//				Vector3 movCam = new Vector3 (horizontal, vertical, speed);
//				transform.Translate (movCam);
#endif
				#if UNITY_IPHONE

				if (Input.touchCount > 0 && 
						Input.GetTouch (0).phase == TouchPhase.Moved) {
						Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;

						Vector3 movCam = new Vector3 (-touchDeltaPosition.x * Time.deltaTime * offSetDelta, 
			                              -touchDeltaPosition.y * Time.deltaTime * offSetDelta, 0);

//						if (transform.position.x < 1 && transform.position.x > -1 && transform.position.y < 1 && transform.position.y > -1) 
						transform.Translate (movCam);

				} 
//		else {
//						Vector3 currPos = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
//						Vector3 vSet = new Vector3 (0, 0, transform.position.z);
//						transform.position = Vector3.Lerp (currPos, vSet, Time.deltaTime);
//				}
#endif
		}
	
	
}
