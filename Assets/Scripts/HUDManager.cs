﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;




public class HUDManager : MonoBehaviour
{

		public Camera camera;
		private float _camX;
		private float _camY;
		private float _camZ;
	
		//HUDs
		public GameObject hudSquare ;
		public int nHUDSquare;
		public List<GameObject> hudSquareArray = new List<GameObject> ();
		public float posYsquare ; //0.6
		public float posXsquareOffset;//0.28
		public float posXsquareBetween;//0.14
		public float square_depth;//-18.8

		public float selectedItemSizeScale;//0.3

	

		public void throwHUDSquare ()
		{
		
				for (int i=0; i< nHUDSquare; i++) {
			
						Vector3 HUDsquarePos = new Vector3 ((_camX - posXsquareOffset) + posXsquareBetween * i, _camY + posYsquare, square_depth);
						GameObject square = Instantiate (hudSquare, HUDsquarePos, Quaternion.identity) as GameObject;
						square.transform.parent = GameObject.Find ("HUD").transform;
						hudSquareArray.Add (square);
				}
		
		}

		//pone objeto en el hud
	
		public void moveToHUD (GameObject g, GameObject HUD, GameObject itemManagerObj)
		{
				//lo mete en el array
				itemManagerObj.GetComponent<itemManager> ().hud.Add (g);

		
				for (int i = 0; i< itemManagerObj.GetComponent<itemManager> ().hud.Count; i++) {

						float xPos = hudSquareArray [i].transform.position.x;
						float yPos = hudSquareArray [i].transform.position.y;
						float zPos = hudSquareArray [i].transform.position.z - 0.01f;

						Vector3 squarePos = new Vector3 (xPos, yPos, zPos);
						Vector3 scale = new Vector3 (selectedItemSizeScale, selectedItemSizeScale);

						g.transform.position = squarePos;
						g.transform.localScale = scale;
						g.GetComponent<cScript> ().active = false;
						g.GetComponent<SphereCollider> ().enabled = false;
			

			
				}
		}
}
